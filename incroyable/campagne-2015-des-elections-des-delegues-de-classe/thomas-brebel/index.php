<!doctype html>

<!--	Et oui oui oui son nom c'est Thomas Brebel !  	-->

<html>

	<head>
		<meta charset="UTF-8" />
		<meta name="description" content="L'avenir est entre vos mains." />
		<title>Thomas Brebel | Campagne 2015 des Élections des Délégués de classe</title>
		<link rel="icon" type="image/png" href="../assets/img/t_thomas.png" />
		<link rel="stylesheet" type="text/css" href="../president.css" />
	</head>
	
	<body>
		<div id="header">
			<div id="banner"></div>
			<div id="lol">Campagne 2015 des Élections des Délégués de classe</div>
		</div>
		<div id="saucisseintroduction">
			<div id="mdr"><a id="saucisselien" href="../">Retourner aux listes</a>
			<div id="saucissetitre">Thomas Brebel, Amicale Élitiste de France</div>
			<div id="saucisseintro">Thomas Brebel est le candidat de l'Amicale Élitiste de France. En axant son programme sur les promesses de privilèges aux filières SI et SVT et la suppression des filières non-SI et non-SVT, Thomas Brebel veut créer la nouvelle élite de la nation, celle qui relèvera le défi de sauver la France.
			</div></div>
			<img src="affiche.png" id="affiche" />
		</div>
	</body>

</html>