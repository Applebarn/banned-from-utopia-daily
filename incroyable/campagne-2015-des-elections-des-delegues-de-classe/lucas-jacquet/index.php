<!doctype html>

<!--	Et oui oui oui son nom c'est Lucas Jacquet !  	-->

<html>

	<head>
		<meta charset="UTF-8" />
		<meta name="description" content="L'avenir est entre vos mains." />
		<title>Lucas Jacquet | Campagne 2015 des Élections des Délégués de classe</title>
		<link rel="icon" type="image/png" href="../assets/img/t_lucas.png" />
		<link rel="stylesheet" type="text/css" href="../president.css" />
	</head>
	
	<body>
		<div id="header">
			<div id="banner"></div>
			<div id="lol">Campagne 2015 des Élections des Délégués de classe</div>
		</div>
		<div id="saucisseintroduction">
			<div id="mdr"><a id="saucisselien" href="../">Retourner aux listes</a>
			<div id="saucissetitre">Lucas Jacquet, Gouvernement de la Classe</div>
			<div id="saucisseintro">Un nouvel air souffle sur la 1ère S1, et cet air pourrait bien être celui du Gouvernement de la Classe, parti crée par Lucas Jacquet, qui est aussi son candidat. Grâce à ses lunettes "porte-bonheur", Lucas Jacquet aspire à être élu délégué afin de rajouter de la classe à la... classe.
			</div></div>
			<img src="affiche.png" id="affiche" />
		</div>
	</body>

</html>