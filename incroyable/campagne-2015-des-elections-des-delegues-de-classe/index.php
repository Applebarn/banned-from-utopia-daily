<!doctype html>

<!--	Oui oui oui oui oui !	-->

<html>

	<head>
		<meta charset="UTF-8" />
		<meta name="description" content="L'avenir est entre vos mains." />
		<title>Campagne 2015 des Élections des Délégués de classe</title>
		<link rel="icon" type="image/png" href="assets/img/jesuisleroidelafrance.png" />
		<link rel="stylesheet" type="text/css" href="president.css" />
	</head>
	
	<body>
		<div id="header">
			<div id="banner"></div>
			<div id="lol">Campagne 2015 des Élections des Délégués de classe</div>
		</div>
		<audio src="assets/music/la_marseillaise.ogg" autoplay loop controls id="marseillepeucher"></audio>
		<div id="superintroduction">
			<div id="supertitle">L'HEURE DU CHOIX</div>
			<div id="superintro">
			Aujourd'hui, 
			<?php
				$vagin = date("j");
				$suce = date("w");
				$couilles = date("n");
				$bite = date("Y");
				$espace = " ";
				if ($vagin == 1) {$jour = "1er";}
				if ($vagin != 1) {$jour = $vagin;}
				if ($suce == 0) {$jourlol = "dimanche";}
				if ($suce == 1) {$jourlol = "lundi";}
				if ($suce == 2) {$jourlol = "mardi";}
				if ($suce == 3) {$jourlol = "mercredi";}
				if ($suce == 4) {$jourlol = "jeudi";}
				if ($suce == 5) {$jourlol = "vendredi";}
				if ($suce == 6) {$jourlol = "samedi";}
				if ($couilles == 1) {$mois = "janvier";}
				if ($couilles == 2) {$mois = "février";}
				if ($couilles == 3) {$mois = "mars";}
				if ($couilles == 4) {$mois = "avril";}
				if ($couilles == 5) {$mois = "mai";}
				if ($couilles == 6) {$mois = "juin";}
				if ($couilles == 7) {$mois = "juillet";}
				if ($couilles == 8) {$mois = "août";}
				if ($couilles == 9) {$mois = "septembre";}
				if ($couilles == 10) {$mois = "octobre";}
				if ($couilles == 11) {$mois = "novembre";}
				if ($couilles == 12) {$mois = "décembre";}
				echo $jourlol.$espace.$jour.$espace.$mois.$espace.$bite;
			?>, 34 élèves de la 1ère S1 sont appelés à voter pour élire leurs deux représentants, nommés Délégués de classe. Ces derniers font l'interface entre les élèves de leur classe et les adultes chargés de leur scolarité. Actuellement, 12 élèves se sont déclarés candidats pour la fonction de Délégué de classe. Cette élection joue un rôle capital, car elle détermine non seulement l'avenir de la 1ère S1, mais aussi de tout le Lycée Maupertuis. Ci-dessous, vous pouvez consulter les différents candidats et un résumé d'eux digne des Sims afin de faire votre choix.<br /><br />Et surtout, n'oubliez pas d'aller voter !
			</div>
			<a class="liste gurven" href="gurven-bourreau">
				<img src="assets/img/t_gurven.png" class="listeimg" /><div class="listetexte">Gurven Bourreau, Mouvement Réformateur</div>
			</a>
			<a class="liste thomas" href="thomas-brebel">
				<img src="assets/img/t_thomas.png" class="listeimg" /><div class="listetexte">Thomas Brebel, Amicale Élitiste de France</div>
			</a>
			<a class="liste arthur" href="arthur-goger">
				<img src="assets/img/t_arthur.png" class="listeimg" /><div class="listetexte">Arthur Goger, Nouvelle Écologie</div>
			</a>
			<a class="liste lucas" href="lucas-jacquet">
				<img src="assets/img/t_lucas.png" class="listeimg" /><div class="listetexte">Lucas Jacquet, Gouvernement de la Classe</div>
			</a>
			<a class="liste marine" href="marine-leveque">
				<img src="assets/img/t_marine.png" class="listeimg" /><div class="listetexte">Marine Lévêque, Parti Pédophile Français</div>
			</a>
			<a class="liste pierrick" href="pierrick-marliac">
				<img src="assets/img/t_pierrick.png" class="listeimg" /><div class="listetexte">Pierrick Marliac, Union de Gauche</div>
			</a>
			<a class="liste louise" href="louise-moysan">
				<img src="assets/img/t_louise.png" class="listeimg" /><div class="listetexte">Louise Moysan, Parti Gaulois</div>
			</a>
			<a class="liste hugo" href="hugo-payen">
				<img src="assets/img/t_hugo.png" class="listeimg" /><div class="listetexte">Hugo Payen, Front Communiste</div>
			</a>
			<a class="liste jarod" href="jarod-perreaut">
				<img src="assets/img/t_jarod.png" class="listeimg" /><div class="listetexte">Jarod Perreaut, Alliance Utopiste</div>
			</a>
			<a class="liste thibo" href="thibo-perrin">
				<img src="assets/img/t_thibo.png" class="listeimg" /><div class="listetexte">Thibo Perrin, Combat Capitaliste</div>
			</a>
			<a class="liste charles" href="charles-pichonnier">
				<img src="assets/img/t_charles.png" class="listeimg" /><div class="listetexte">Charles Pichonnier, Centre au Milieu</div>
			</a>
			<a class="liste meven" href="meven-vaysset">
				<img src="assets/img/t_meven.png" class="listeimg" /><div class="listetexte">Meven Vaysset, Ligue du Sud</div>
			</a><br />
		</div>
	</body>

</html>