<!doctype html>

<!--	Et oui oui oui son nom c'est Pierrick Marliac !  	-->

<html>

	<head>
		<meta charset="UTF-8" />
		<meta name="description" content="L'avenir est entre vos mains." />
		<title>Pierrick Marliac | Campagne 2015 des Élections des Délégués de classe</title>
		<link rel="icon" type="image/png" href="../assets/img/t_pierrick.png" />
		<link rel="stylesheet" type="text/css" href="../president.css" />
	</head>
	
	<body>
		<div id="header">
			<div id="banner"></div>
			<div id="lol">Campagne 2015 des Élections des Délégués de classe</div>
		</div>
		<div id="saucisseintroduction">
			<div id="mdr"><a id="saucisselien" href="../">Retourner aux listes</a>
			<div id="saucissetitre">Pierrick Marliac, Union de Gauche</div>
			<div id="saucisseintro">Pierrick Marliac se veut rassembleur. Candidat naturel de l'Union de Gauche, inspiré par les grands noms de l'URSS, comme Lenine ou Staline, Pierrick compte bien faire vivre une nouvelle ère de communisme pur et dur à la 1ère S1.
			</div></div>
			<img src="affiche.png" id="affiche" />
		</div>
	</body>

</html>