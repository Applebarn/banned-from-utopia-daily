<!doctype html>

<!--	Et oui oui oui son nom c'est Meven Vaysset !  	-->

<html>

	<head>
		<meta charset="UTF-8" />
		<meta name="description" content="L'avenir est entre vos mains." />
		<title>Meven Vaysset | Campagne 2015 des Élections des Délégués de classe</title>
		<link rel="icon" type="image/png" href="../assets/img/t_meven.png" />
		<link rel="stylesheet" type="text/css" href="../president.css" />
	</head>
	
	<body>
		<div id="header">
			<div id="banner"></div>
			<div id="lol">Campagne 2015 des Élections des Délégués de classe</div>
		</div>
		<div id="saucisseintroduction">
			<div id="mdr"><a id="saucisselien" href="../">Retourner aux listes</a>
			<div id="saucissetitre">Meven Vaysset, Ligue du Sud</div>
			<div id="saucisseintro">Meven Vaysset, candidate d'office de la Ligue du Sud, compte faire gagner son parti en changeant radicalement de discours. En adoptant un ton plus proche du peuple et en faisant jouer les bas instincts des humains, Meven Vaysset espère être élue déléguée et faire rentrer la France dans 1000 ans de ténèbres.
			</div></div>
			<img src="affiche.jpg" id="affiche" />
		</div>
	</body>

</html>