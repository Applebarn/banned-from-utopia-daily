<!doctype html>

<!--	Et oui oui oui son nom c'est Arthur Goyer !  	-->

<html>

	<head>
		<meta charset="UTF-8" />
		<meta name="description" content="L'avenir est entre vos mains." />
		<title>Arthur Goger | Campagne 2015 des Élections des Délégués de classe</title>
		<link rel="icon" type="image/png" href="../assets/img/t_arthur.png" />
		<link rel="stylesheet" type="text/css" href="../president.css" />
	</head>
	
	<body>
		<div id="header">
			<div id="banner"></div>
			<div id="lol">Campagne 2015 des Élections des Délégués de classe</div>
		</div>
		<div id="saucisseintroduction">
			<div id="mdr"><a id="saucisselien" href="../">Retourner aux listes</a>
			<div id="saucissetitre">Arthur Goger, Nouvelle Écologie</div>
			<div id="saucisseintro">Arthur Goger, surnommé "Tutur" dans le dialecte local, est le seul candidat écologiste de cette élection. En promettant notemment de la "weeeeeed" et du "shit" aux élèves, "Tutur" espère bien se faire élire à la majorité absolue. Et en plus il incarne le "turfu", ce qui le rend tout à fait légitime à être élu délégué.
			</div></div>
			<img src="affiche.png" id="affiche" />
		</div>
	</body>

</html>