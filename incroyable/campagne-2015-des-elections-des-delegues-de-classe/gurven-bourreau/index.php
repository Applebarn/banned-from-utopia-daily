<!doctype html>

<!--	Et oui oui oui son nom c'est Gurven Bourreau !  	-->

<html>

	<head>
		<meta charset="UTF-8" />
		<meta name="description" content="L'avenir est entre vos mains." />
		<title>Gurven Bourreau | Campagne 2015 des Élections des Délégués de classe</title>
		<link rel="icon" type="image/png" href="../assets/img/t_gurven.png" />
		<link rel="stylesheet" type="text/css" href="../president.css" />
	</head>
	
	<body>
		<div id="header">
			<div id="banner"></div>
			<div id="lol">Campagne 2015 des Élections des Délégués de classe</div>
		</div>
		<div id="saucisseintroduction">
			<div id="mdr"><a id="saucisselien" href="../">Retourner aux listes</a>
			<div id="saucissetitre">Gurven Bourreau, Mouvement Réformateur</div>
			<div id="saucisseintro">Gurven Bourreau incarne le renouveau, la réforme. Désigné comme candidat par son parti politique, le Mouvement Réformateur, Gurven Bourreau se présente comme une alternative aux deux délégués en cours de mandat, à savoir Thibo Perrin et Marianne Nédélec. Son programme s'axe notamment sur l'évolution de l'éducation ainsi que une réforme des filières.
			</div></div>
			<img src="affiche.png" id="affiche" />
		</div>
	</body>

</html>