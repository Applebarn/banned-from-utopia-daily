<!doctype html>

<!--	Et oui oui oui son nom c'est Charles Pichonnier !  	-->

<html>

	<head>
		<meta charset="UTF-8" />
		<meta name="description" content="L'avenir est entre vos mains." />
		<title>Charles Pichonnier | Campagne 2015 des Élections des Délégués de classe</title>
		<link rel="icon" type="image/png" href="../assets/img/t_charles.png" />
		<link rel="stylesheet" type="text/css" href="../president.css" />
	</head>
	
	<body>
		<div id="header">
			<div id="banner"></div>
			<div id="lol">Campagne 2015 des Élections des Délégués de classe</div>
		</div>
		<div id="saucisseintroduction">
			<div id="mdr"><a id="saucisselien" href="../">Retourner aux listes</a>
			<div id="saucissetitre">Charles Pichonnier, Centre au Milieu</div>
			<div id="saucisseintro">Il nous fallait bien un centriste, le voici ! Charles Pichonnier incarne la France profonde, la France des campagnes. La France qui sent bon le fumier. En piquant des idées de tous les autres candidats, Charles Pichonnier espère créer un équilibre parfait entre la gauche, la droite, et le cosmos.
			</div></div>
			<img src="affiche.png" id="affiche" />
		</div>
	</body>

</html>