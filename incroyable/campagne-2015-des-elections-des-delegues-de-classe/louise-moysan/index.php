<!doctype html>

<!--	Et oui oui oui son nom c'est Louise Moysan !  	-->

<html>

	<head>
		<meta charset="UTF-8" />
		<meta name="description" content="L'avenir est entre vos mains." />
		<title>Louise Moysan | Campagne 2015 des Élections des Délégués de classe</title>
		<link rel="icon" type="image/png" href="../assets/img/t_louise.png" />
		<link rel="stylesheet" type="text/css" href="../president.css" />
	</head>
	
	<body>
		<div id="header">
			<div id="banner"></div>
			<div id="lol">Campagne 2015 des Élections des Délégués de classe</div>
		</div>
		<div id="saucisseintroduction">
			<div id="mdr"><a id="saucisselien" href="../">Retourner aux listes</a>
			<div id="saucissetitre">Louise Moysan, Parti Gaulois</div>
			<div id="saucisseintro">Louise Moysan n'aime pas la France moderne. Afin de révolutionner cette France et lui faire revivre sa gloire d'antan, Louise Moysan prône à travers son association politique, le Parti Gaulois, un retour à l'époque où la France était encore la Gaule.
			</div></div>
			<img src="affiche.png" id="affiche" />
		</div>
	</body>

</html>