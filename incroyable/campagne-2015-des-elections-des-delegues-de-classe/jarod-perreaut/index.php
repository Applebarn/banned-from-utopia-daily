<!doctype html>

<!--	Et oui oui oui son nom c'est Jarod Perreaut !  	-->

<html>

	<head>
		<meta charset="UTF-8" />
		<meta name="description" content="L'avenir est entre vos mains." />
		<title>Jarod Perreaut | Campagne 2015 des Élections des Délégués de classe</title>
		<link rel="icon" type="image/png" href="../assets/img/t_jarod.png" />
		<link rel="stylesheet" type="text/css" href="../president.css" />
	</head>
	
	<body>
		<div id="header">
			<div id="banner"></div>
			<div id="lol">Campagne 2015 des Élections des Délégués de classe</div>
		</div>
		<div id="saucisseintroduction">
			<div id="mdr"><a id="saucisselien" href="../">Retourner aux listes</a>
			<div id="saucissetitre">Jarod Perreaut, Alliance Utopiste</div>
			<div id="saucisseintro">Jarod Perreaut défend des convictions et des idées encore inconnues à l'heure actuelle. Il promet juste "qu'il va changer la France en mieux" car ça ne peut pas être pire selon lui. Il est clair que son affiche est destiné à un électorat de niche, mais personne ne sait pourquoi.
			</div></div>
			<img src="affiche.png" id="affiche" />
		</div>
	</body>

</html>