<!doctype html>

<!--	Et oui oui oui son nom c'est Thibo Perrin !  	-->

<html>

	<head>
		<meta charset="UTF-8" />
		<meta name="description" content="L'avenir est entre vos mains." />
		<title>Thibo Perrin | Campagne 2015 des Élections des Délégués de classe</title>
		<link rel="icon" type="image/png" href="../assets/img/t_thibo.png" />
		<link rel="stylesheet" type="text/css" href="../president.css" />
	</head>
	
	<body>
		<div id="header">
			<div id="banner"></div>
			<div id="lol">Campagne 2015 des Élections des Délégués de classe</div>
		</div>
		<div id="saucisseintroduction">
			<div id="mdr"><a id="saucisselien" href="../">Retourner aux listes</a>
			<div id="saucissetitre">Thibo Perrin, Combat Capitaliste</div>
			<div id="saucisseintro">Il incarne le SW4G et la Youtube Money à lui tout seul. Candidat naturel de son parti, le Combat Capitaliste, et candidat à sa réélection, Thibo Perrin compte bien répandre la bonne parole du capitalisme dans le coeur de chaque élève de 1ère S1, notamment en promettant 1€ à toute personne qui vote pour lui.
			</div></div>
			<img src="affiche.png" id="affiche" />
		</div>
	</body>

</html>