<!doctype html>

<!--	Et oui oui oui son nom c'est Hugo Payen !  	-->

<html>

	<head>
		<meta charset="UTF-8" />
		<meta name="description" content="L'avenir est entre vos mains." />
		<title>Hugo Payen | Campagne 2015 des Élections des Délégués de classe</title>
		<link rel="icon" type="image/png" href="../assets/img/t_hugo.png" />
		<link rel="stylesheet" type="text/css" href="../president.css" />
	</head>
	
	<body>
		<div id="header">
			<div id="banner"></div>
			<div id="lol">Campagne 2015 des Élections des Délégués de classe</div>
		</div>
		<div id="saucisseintroduction">
			<div id="mdr"><a id="saucisselien" href="../">Retourner aux listes</a>
			<div id="saucissetitre">Hugo Payen, Front Communiste</div>
			<div id="saucisseintro">Hugo Payen représente selon lui "le vrai communisme", celui qui ne laisse "tomber personne". En axant son programme à l'opposé du programme de l'Amicale Élitiste de France, qu'il considère digne du nazisme, Hugo Payen espère bien sauver la 1ère S1 d'un désastre imminent.
			</div></div>
			<img src="affiche.jpg" id="affiche" />
		</div>
	</body>

</html>